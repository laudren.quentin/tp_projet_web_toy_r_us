<?php
session_start();

// Constante de chemin
define('DS', DIRECTORY_SEPARATOR);  
// Allias pratique pour écrire les chemins plus court
define('PATH_ROOT', __DIR__ . DS); /// Contient le chemin fichier absolu de la racine du projet

// Contrôle de la connexion à la base de donnée

define('DB_HOST', 'database');
define('DB_USER', 'lamp');
define('DB_PASS', 'lamp');
define('DB_NAME', 'lamp');

//sécurisation des input pour éviter une injection de script
function verifyInput($inputEntry) {
        $inputEntry = trim($inputEntry);
        $inputEntry = stripcslashes($inputEntry);
        $inputEntry = htmlspecialchars($inputEntry);
        return $inputEntry;
    }

$bdd = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_query($bdd, "SET CHARACTER SET 'utf8'");

if (!$bdd) {
    // si connection ne marche pas
    echo ('Erreur, impossible de se connecter à la base de donné');
} // si la connexion est établie on peut lancer la suite

require_once PATH_ROOT . 'inc' . DS . 'functions.php';
require_once PATH_ROOT . 'app' . DS . 'rooter.php';
routerStart(); // Lance l'app
 // Fermeture de l'accès
 mysqli_close( $bdd );
