<?php

/*
 * Function de converstion String en Float puis en format monaitaire 
 */
function toEuroFormatConverter($price) {
    $float_price = floatval($price);
    $fmt = numfmt_create( 'de_DE', NumberFormatter::CURRENCY );
    return numfmt_format_currency($fmt, $float_price, "EUR");
}


/**
 * 
 */
function queryMysqlAuto(string $query, mysqli $bdd, string $get = '', string $type):array 
{
    $arr = [];
    if ($stmt = mysqli_prepare($bdd, $query)) {
        if ($get !== '' ) {
            $getRequest = $_GET[$get];
        }
        if (mysqli_stmt_bind_param($stmt, $type, $getRequest)) {
            // Exécution puis récupération du résultat
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            // Fermeture de la commande
            mysqli_stmt_close($stmt);
            if ($result) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $arr[] = $row;
                }
               
                return $arr;
            }
        }
    }
}