<?php

function routerStart():void
{
    // Si REDIRECT_URL n'existe pas on met la valeur '/'
    $route = $_SERVER['REDIRECT_URL'] ?? '/';

    switch( $route ){
        case '/': // cas de base
            require_once PATH_ROOT . 'app' . DS . 'pages' . DS .'functions' . DS . 'homeFunction.php';
            hommeRender();
            break;
            
        case '/NosJouets': // page liste
            require_once PATH_ROOT . 'app' . DS . 'pages' . DS .'functions' . DS . 'toysListFunction.php';
            toysListRender();
            break;
        case '/DetailDuJouet': // page détail d'un jouet
            require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'functions' . DS . 'toyDetailFunction.php';
            toyDetailRender();
            break;
        default: // page 404 pour le reste
            http_response_code(404); 
            require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'functions' . DS . '404Function.php';
            error404Render();
            break;
    }

}