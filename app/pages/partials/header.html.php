<?php global $bdd; ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="app/pages/partials/css/style.css">
    <link rel="shortcut icon" href="/img/favicon.png" />


    <title><?php echo $pageTitle ?> - Toys'R'us</title>
</head>


<body>
    <div class="containerAll">

        <header>
            <a href="/" id="logo"><img src="./img/Logo.png" alt="Logo Toys'R'us"></a>
            <nav>
                <ul>
                    <li><a href="./NosJouets">Tous les jouets</a></li>
                    <li>
                        <div class="brandsMenu">Par marque</div>
                        <ul>
                            <?php
                            $brandsArray = mysqli_query($bdd, "SELECT b.*, COUNT(t.brand_id) as nbrToys, t.brand_id 
                                                            FROM toys AS t INNER JOIN brands AS b ON t.brand_id=b.id 
                                                            GROUP BY brand_id;");
                            while ($dataBrands = mysqli_fetch_assoc($brandsArray)) {
                            ?>
                                <li>
                                    <a href="/NosJouets?brand=<?php echo $dataBrands['id'] ?>" id="marque<?php echo $dataBrands['id'] ?>">
                                        <?php echo $dataBrands['name']; ?> <span>(<?php echo $dataBrands['nbrToys'] ?>)</span>
                                    </a>
                                </li>
                            <?php
                            } ?>
                        </ul>
                    </li>
                    <!-- <li><a href="./">Top 3 Des Ventes</a></li> // Lien vers le top3 -->
                </ul>
                
            </nav>
        </header>
        <main>

    
    
    