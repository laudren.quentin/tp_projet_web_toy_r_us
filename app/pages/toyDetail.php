<?php



/* Tableau Des info des Jouets avec le nom de la marque selon l'id du toy en GET*/
$toyAndBrand = mysqli_fetch_assoc(mysqli_query($bdd, 'SELECT t.id, t.name, t.description, t.price, t.image, b.name AS marque 
                                                        FROM brands AS b INNER JOIN toys AS t ON b.id = t.brand_id
                                                        WHERE t.id=' . $SecureGetToy . ';'));
?>


<!-- Création de l'affichage du produit -->
<div class="detailContainer">
    <h2><?php echo $toyAndBrand['name'] ?></h2>
    <div class="detailFlexPart">
        <div class="detailLeftPart">
            <div>
                <img src="img/<?php echo $toyAndBrand['image'] ?>" alt="<?php echo $toyAndBrand['name'] ?>">
            </div>
            <p class="detailPrice"><?php echo toEuroFormatConverter( $toyAndBrand['price'] ) ?></p>

            <!-- SELECTEUR DU MAGASIN POUR STOCK -->
            <form method="POST" action="">
                <select name='store' id="store">
                    <!-- nom de la clef Store dans POST -->
                    <option value="">Quelle magasin ?</option>
                    <?php
                    $store = mysqli_query($bdd, "SELECT st.id, st.name FROM stores AS st;");
                    while ($storeData = mysqli_fetch_assoc($store)) { ?>
                        <option value="<?php echo $storeData['id'] ?>"> <?php echo $storeData['name']; ?> </option>
                    <?php
                    }
                    ?>
                </select>
                
                <input type="submit" value='ok'></input>
            </form>

            <?php stockQuantity() // Affiche les stocks selon $_POST['store'] ?>
        </div>
        <div class="detailRightPart">
            <p class="detailMarque">Marque: <strong><?php echo $toyAndBrand['marque'] ?></strong></p>
            <p><?php echo $toyAndBrand['description'] ?></p>
        </div>
    </div>
</div>