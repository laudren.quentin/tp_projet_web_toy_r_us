<?php


function error404Render()
{
    $pageTitle = '404 NotFound';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.html.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . '404.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.html.php';
}



