<?php

function toyDetailRender()
{
    $pageTitle = 'Detail du jouet';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.html.php';

    if (!empty($_GET['toy']) && intval($_GET['toy'])) {

        $SecureGetToy = verifyInput($_GET['toy']);
        //Requête pour vérifier que l'id Du jouet existe dans la bdd après s'être assuré qu'il existait
        $TestToysID = mysqli_fetch_assoc(mysqli_query($bdd, 'SELECT EXISTS (SELECT toys.id FROM toys WHERE id=' . $SecureGetToy . ') AS Exist;'));
        if ($TestToysID['Exist']) {

            require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'toyDetail.php';
        } else {
            header('Location: /NotFound');
        }
    } else {
        header('Location: /NotFound');
    }
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.html.php';
}


function stockQuantity()
{
    global $bdd;
    $getToy = verifyInput($_GET['toy']);

    
    if (!empty($_POST['store']) && intval($_POST['store'])) {
        $getStore = verifyInput($_POST['store']); // Sécu du POST dans les reqêtes    

        $idStoreExist = mysqli_fetch_assoc(mysqli_query(
            $bdd,
            'SELECT EXISTS (
                SELECT stores.id 
                FROM stores 
                WHERE id=' . $getStore . '
            ) 
            AS Exist;'
        ));

        if ($idStoreExist['Exist']) { // Si il existe dans la bdd

            $totalStock = mysqli_fetch_assoc(mysqli_query(
                $bdd,
                "SELECT  s.quantity, s.toy_id, s.store_id, stores.id, stores.name 
                FROM stock AS s INNER JOIN stores ON s.store_id = stores.id 
                WHERE stores.id=" . $getStore . " and s.toy_id=" . $getToy . ";"
            ));

            echo '<p>' . $totalStock['name'] . '</p>';
            echo '<p class="stockText">Stock: <span class="stockInt">' . $totalStock['quantity'] . '</span></p>';
        }
    } else {
        $totalStock = mysqli_fetch_assoc(mysqli_query(
            $bdd,
            "SELECT SUM(quantity) AS quantity FROM stock 
            WHERE toy_id=" . $getToy . ";"
        ));

        echo '<p>Total</p>';
        echo '<p class="stockText">Stock: <span class="stockInt">' . $totalStock['quantity'] . '</span></p>';
    }
}
