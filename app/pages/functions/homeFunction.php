<?php 


function hommeRender()
{
    $pageTitle = 'Accueil';
    if(empty($_GET) && empty($_POST)){ // Si POST ET GET vide on affiche la page
        require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.html.php';
        require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'home.php';
        require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.html.php';
    }
    else{
        header('Location: /NotFound');
    }

}

/**
 * Permet grâce à une requête MYSQLI d'afficher une Item Card
 * @param mysqli_query($query)
 */
function cardItemTopSale($requestSQL){
    echo ""?>
    <div class="container_list"> <?php
    $count = 1;
        while($dataRow = mysqli_fetch_assoc($requestSQL)){ ?>
        <div class='product_card' id='<?php echo $dataRow['id']?>' >
        <p style="font-size: 1.4rem; font-weight: 600;">N° <?php echo $count ++?></p>
            <img src="img/<?php echo $dataRow['image']?>" alt="<?php echo $dataRow['name']?>">
            <a href="./DetailDuJouet?toy=<?php echo  $dataRow['id']?>"><?php echo $dataRow['name'] ?></a>
            <p><?php echo toEuroFormatConverter( $dataRow['price'] ) ?></p> 
        </div>
        <?php
        } ?> 
    </div>
<?php ;
}