<?php


function toysListRender()
{
    $pageTitle = 'Nos jouets';
    global $bdd;

    //liste de tout les jouet avec filtre par prix croissant / décroissant
    if(empty($_GET) || !empty($_GET['order']) && $_GET['order'] == '1' || !empty($_GET['order']) && $_GET['order'] == '2' ){

        $listArr = mysqli_query($bdd, 'SELECT * FROM toys;');
        if( !empty($_GET['order']) && $_GET['order'] == '1'){ // Croissant 
            $listArr = mysqli_query($bdd, 'SELECT * FROM toys ORDER BY price ASC;');
        }
        else if( !empty($_GET['order']) && $_GET['order'] == '2'){ // Décroissant
            $listArr = mysqli_query($bdd, 'SELECT * FROM toys ORDER BY price DESC;');
        }
    }

    // Si une marque est selectionné
    if(!empty($_GET['brand'])){
        $toysResultat = 
        'SELECT toys.brand_id, toys.name, toys.image, toys.price, toys.id  
        FROM toys WHERE brand_id =? ;';

    if(!empty($_GET['order']) && $_GET['order'] =='1'){ // Croissant 
        $toysResultat = 
        'SELECT toys.brand_id, toys.name, toys.image, toys.price, toys.id  
        FROM toys WHERE brand_id =? 
        ORDER BY toys.price ASC;';
    }
    elseif(!empty($_GET['order']) && $_GET['order'] == '2'){ // Décroissant
        $toysResultat = 
        'SELECT toys.brand_id, toys.name, toys.image, toys.price, toys.id  
        FROM toys WHERE brand_id =? 
        ORDER BY toys.price DESC;';
    }

    // Fonction qui prépare la requête
    $listArr = queryMysqlAuto($toysResultat, $bdd, 'brand', 'i');

    }

    if($listArr){
        
            require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.html.php';
            require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'toysList.php';
            require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.html.php';
    }else{
        header('Location: /NotFound');
    }
}


function cardItem($requestSQL)
{
    echo "" ?>
    <div class="container_list"> 
        <?php foreach ($requestSQL as $dataRow) { ?>
            
            <div class='product_card' id='<?php echo $dataRow['id'] ?>'>
                <img src="img/<?php echo $dataRow['image'] ?>" alt="<?php echo $dataRow['name'] ?>">
                <a href="./DetailDuJouet?toy=<?php echo  $dataRow['id'] ?>"><?php echo $dataRow['name'] ?></a>
                <p><?php echo toEuroFormatConverter($dataRow['price']) ?></p>
            </div>
        <?php
        } ?>
    </div>
<?php
}




/* Fin de l'usine à gaz */